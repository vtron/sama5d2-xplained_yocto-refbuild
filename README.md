# SAMA5D2 Development Kit - Reference Buildroot OS Build

This repository includes instructions (following), an additional openembedded layer, and helper scripts for building and deploying an OS to the SAMA5D2 Xplained development kit.
The following URL is the link to the home of the development kit - it includes user guides, schematics etc
https://www.microchip.com/DevelopmentTools/ProductDetails/ATSAMA5D2C-XULT

# Instructions for building the reference OS

The build requires a linux platform (recommended Ubuntu based 16.04 / 18.04). This can be a Virtual Machine, or a Docker Container.
This repository includes a Dockerfile for preparing a Ubuntu-18.04 container to perform the build. The instructions for creating and using the Docker container are further below.

From: https://www.at91.com/linux4sam/bin/view/Linux4SAM/BuildRootBuild
using toolchain 7.4.1: https://releases.linaro.org/components/toolchain/binaries/latest-7/arm-linux-gnueabihf/

Prepare
```
# clone buildroot for at91
git clone https://github.com/linux4sam/buildroot-at91.git
# checkout the required branch
( cd buildroot-at91/ && git checkout linux4sam_6.0 -b buildroot-at91-linux4sam_6.0 )

# clone this repo
git clone https://bitbucket.org/vtron/sama5d2-xplained_yocto-refbuild.git

# Get the pre-built toolchain
wget https://releases.linaro.org/components/toolchain/binaries/latest-7/arm-linux-gnueabihf/gcc-linaro-7.4.1-2019.02-x86_64_arm-linux-gnueabihf.tar.xz
# extract toolchain
tar xJf gcc-linaro-7.4.1-2019.02-x86_64_arm-linux-gnueabihf.tar.xz
```

Configure
```
# configure
BR2_EXTERNAL=../sama5d2-xplained_yocto-refbuild/buildroot-external-sama5d2-xplained-overlay/ make sama5d2-xplained-overlay_defconfig

## to make any modifications, use the make menuconfig:
# make any other customisations
BR2_EXTERNAL=../sama5d2-xplained_yocto-refbuild/buildroot-external-sama5d2-xplained-overlay/ make menuconfig

# export a clean defconfig file that only includes the non-default settings
BR2_EXTERNAL=../sama5d2-xplained_yocto-refbuild/buildroot-external-sama5d2-xplained-overlay/ make savedefconfig BR2_DEFCONFIG=<path-to-output_defconfig>
```

Build
```
BR2_EXTERNAL=../sama5d2-xplained_yocto-refbuild/buildroot-external-sama5d2-xplained-overlay/ make

# output image files -> output/image

```

Notes
```
# to clean only the target area & rebuild - eg after removing some packages
rm -rf output/target
find output/ -name ".stamp_target_installed" | xargs rm -rf
```

```
# Customised busybox config - based on buildroot-at91/package/busybox/busybox.config
# diff to see the customisations
diff buildroot-at91/package/busybox/busybox.config sama5d2-xplained_yocto-refbuild/buildroot-external-sama5d2-xplained-overlay/board/refbuild/sama5/busybox.config

```



# SAMA5D2 Development Kit - Reference Openembedded (Yocto / Poky) OS Build

This repository includes instructions (following), an additional openembedded layer, and helper scripts for building and deploying an OS to the SAMA5D2 Xplained development kit.
The following URL is the link to the home of the development kit - it includes user guides, schematics etc
https://www.microchip.com/DevelopmentTools/ProductDetails/ATSAMA5D2C-XULT


# Instructions for building the reference OS

The build requires a linux platform (recommended Ubuntu based 16.04 / 18.04). This can be a Virtual Machine, or a Docker Container.
This repository includes a Dockerfile for preparing a Ubuntu-18.04 container to perform the build. The instructions for creating and using the Docker container are further below.

The build can take several hours to complete.


## Create the Development Tree
Currently this is performed outside of the container, and volume-mounted into the container.
But this could all just be performed inside the container.

```
# create the location to perform the build in (/opt/yocto/sama5d2-xplained). Customise as needed.
sudo mkdir -p /opt/yocto/sama5d2-xplained/source /opt/yocto/sama5d2-xplained/downloads
# change ownership to yourself (change 'user' to your user)
sudo chown -R user:user /opt/yocto

cd /opt/yocto/sama5d2-xplained/source

# Get the reference image yocto overlays and scripts
git clone --recurse-submodules https://bitbucket.org/vtron/sama5d2-xplained_yocto-refbuild.git

# Get the core poky/openembedded and required 3rd party overlays
git clone git://git.yoctoproject.org/poky && \
git clone git://git.openembedded.org/meta-openembedded && \
git clone https://github.com/linux4sam/meta-atmel

# Checkout the appropriate branch - 'thud'. 
( cd poky && git pull && git checkout thud ) && \
( cd meta-openembedded && git pull && git checkout thud ) && \
( cd meta-atmel && git pull && git checkout thud )

```

Now the /opt/yocto/sama5d2-xplained/source should look like:
```
meta-atmel
meta-openembedded
poky
sama5d2-xplained_yocto-refbuild
```

## Build
### Prepare

If using a docker container, start/attach to the container and change to the 'user' to perform the build
```
# Navigate to source dir
cd /opt/yocto/sama5d2-xplained
 
# Create the build tree (note the extra '.' it's important)
. ./source/poky/oe-init-build-env build-sama5d2-xplained
 
# should now be automatically placed in the build-sama5d2-xplained path
 
# create a link to the 'downloads' that will be shared. This so we can share the downloads between builds for other target boards.
ln -s ../downloads
```
Configure the build config files - conf/local.conf & conf/bblayers.conf

Make the following changes to the conf/local.conf file:
```
## after the MACHINE ??= line:
MACHINE = "sama5d2-xplained-sd"

## after the DISTRO ?= "poky" line:
DISTRO = "poky-atmel"

## after the USER_CLASSES ?= "buildstats image-mklibs image-prelink" line:
USER_CLASSES = "buildstats"
 
## add to the end:
LICENSE_FLAGS_WHITELIST += "commercial commercial_gstreamer1.0-plugins-ugly"
# Clean up temporary build files (saves a lot of space)
INHERIT += "rm_work"
```

Make the following changes to the conf/bblayers.conf file:

Add the following layers to the conf/bblayers.conf file BBLAYERS variable (after the meta-yocto-bsp layer)
```
  /opt/yocto/sama5d2-xplained/source/meta-openembedded/meta-oe \
  /opt/yocto/sama5d2-xplained/source/meta-openembedded/meta-multimedia \
  /opt/yocto/sama5d2-xplained/source/meta-openembedded/meta-networking \
  /opt/yocto/sama5d2-xplained/source/meta-openembedded/meta-filesystems \
  /opt/yocto/sama5d2-xplained/source/meta-openembedded/meta-python \
  /opt/yocto/sama5d2-xplained/source/meta-openembedded/meta-webserver \
  /opt/yocto/sama5d2-xplained/source/meta-atmel \
  /opt/yocto/sama5d2-xplained/source/sama5d2-xplained_yocto-refbuild/meta-vetron-sama5d2-xplained-overlay \
```

### Build
Perform the actual build.
We also build the SDK that can be used to build apps for the target device. Documentation for using the SDK is further below.
```
# Navigate to source dir (if not already in the build-sama5d2-xplained path)
cd /opt/yocto/sama5d2-xplained
# Source the build env - only if not already performed in this shell instance
. ./source/poky/oe-init-build-env build-sama5d2-xplained
# should now be in the build-sama5d2-xplained path

# Build - may take a couple hours
bitbake device-image
 
# Build the SDK
bitbake meta-toolchain
```

## Install on an SD Card

The build output provides a root filesystem and all the required artifacts to be installed onto an SD card.

```
A helper script will create an image with multiple partitions that can be directly imaged onto an SD card
cd /opt/yocto/sama5d2-xplained/source/sama5d2-xplained_yocto-refbuild/scripts

# Create an SD card Image - point to where the build was run
sudo ./create-mmc-sdcard-image.sh -b /opt/yocto/sama5d2-xplained/build-sama5d2-xplained/
 
# An image file will be created (sama5d2-xplained-sd.img) that can be directly imaged onto an sdcard, eg:
sudo dd if=sama5d2-xplained-sd.img of=/dev/path-to-SD-device-node bs=1M
```



## Docker build machine setup
```
# Ensure docker services are running

# Create the docker image - run from the location of the Dockerfile
docker build -t yocto-ubuntu18.04 docker/
```

Create a docker container

Note that at this step we want to mount in a location from the host machine. This location is where the full build will be performed, so must have a lot of space.
```
# Create a container called 'yocto-ubuntu18.04' which mounts '/opt/yocto' on the host to '/opt/yocto' in the container.
docker create -t -i -v /opt/yocto:/opt/yocto --name yocto-build-ubuntu18.04 yocto-ubuntu18.04 bash
```

Start the container and prepare to run the build
```
# start the container
docker start yocto-build-ubuntu18.04

# attach a console
docker exec -it yocto-build-ubuntu18.04 /bin/bash

# change to the user 'user' - the build requires that the build is not performed as root
su - user
```


## Using the SDK

When performing the build, we also built 'meta-toolchain'. This creates a script that deploys an independent build environment for the target device. (can be deployed to other Linux distros)

### Install and prepare the SDK
```
# Install the SDK somewhere - eg: /opt/yocto/sama5d2-xplained/sdk-sama5d2-xplained
/opt/yocto/sama5d2-xplained/build-sama5d2-xplained/tmp/deploy/sdk/poky-atmel-glibc-x86_64-meta-toolchain-cortexa5t2hf-neon-toolchain-2.6.1.sh \
    -d /opt/yocto/sama5d2-xplained/sdk-sama5d2-xplained
```

### Rebuilding the Kernel
```
# To build a kernel module, we need to ensure kernel tools built: cd <sdk-install-path>/sysroots/<mach>/usr/src/kernel ( source /opt/yocto/sama5d2-xplained/sdk-sama5d2-xplained/environment-setup-cortexa5hf-neon-poky-linux-gnueabi ; make silentoldconfig scripts )
# Build kernel if required
(
  source /opt/yocto/sama5d2-xplained/sdk-sama5d2-xplained/environment-setup-cortexa5hf-neon-poky-linux-gnueabi ;
  make CROSS_COMPILE=arm-poky-linux-gnueabi- ARCH=arm zImage
)

# Build kernel modules if required
(
  source /opt/yocto/sama5d2-xplained/sdk-sama5d2-xplained/environment-setup-cortexa5hf-neon-poky-linux-gnueabi ;
  make CROSS_COMPILE=arm-poky-linux-gnueabi- ARCH=arm modules
)
 
# To rebuild the device-tree file (dts) for testing config changes
cd <sdk-install-path>/sysroots/<mach>/usr/src/kernel
# ensure the link to the include path exists (specifically for include/dt-bindings)
( cd arch/arm/boot/dts && ln -s ../../../../include/ )
cd <sdk-install-path>/sysroots/<mach>/usr/src/kernel
 
(
  source /opt/yocto/sama5d2-xplained/sdk-sama5d2-xplained/environment-setup-cortexa5hf-neon-poky-linux-gnueabi ;
  make CROSS_COMPILE=arm-poky-linux-gnueabi- ARCH=arm at91-sama5d2_xplained.dtb
)
```

### Building a C or C++ App

The SDK provides all the compilers & libs for building apps.

The simplest way to use the SDK is to source the environment script included with the SDK that exports environment variables.
```
(
  source /opt/yocto/sama5d2-xplained/sdk-sama5d2-xplained/environment-setup-cortexa5hf-neon-poky-linux-gnueabi ;
  make CC="$CC" CXX="$CXX" etc
)
```

Building a CMake based app
```
(
  source /opt/yocto/sama5d2-xplained/sdk-sama5d2-xplained/environment-setup-cortexa5hf-neon-poky-linux-gnueabi ;
  cmake path-to-cmake-project-src/ ;
  make -j$(nproc)
)
```
