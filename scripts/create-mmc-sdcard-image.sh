#!/bin/sh

set -e

SCRIPT_DIR=$(cd $(dirname $0); pwd)

OE_BUILD_DIR=
OUTPUT_FILE=

print_usage() {
    echo "Usage: $0 options"
    echo "  -b OE_BUILD_DIR    : the dir that the OE Build was performed in. Eg: /opt/yocto/sama5d2-xplained/build-sama5d2-xplained"
}

while getopts "b:?" opt; do
    case $opt in
        "b")
            OE_BUILD_DIR=$OPTARG
            ;;
        *)
            print_usage
            exit 1
            ;;
    esac
done

if [ -z "$OE_BUILD_DIR" ]; then
    echo "ERROR : no OE build dir provided"
    echo ""
    print_usage
    exit 1
fi

# Create the image including VFAT partition with files: at91bootstrap, u-boot
$SCRIPT_DIR/create-sd-image.sh \
    -o sama5d2-xplained-sd.img \
    -r ${OE_BUILD_DIR}/tmp/deploy/images/sama5d2-xplained-sd/device-image-sama5d2-xplained-sd.tar.gz \
    \
    -f ${OE_BUILD_DIR}/tmp/deploy/images/sama5d2-xplained-sd/u-boot.bin \
    -f ${OE_BUILD_DIR}/tmp/deploy/images/sama5d2-xplained-sd/BOOT.BIN \
    -f ${OE_BUILD_DIR}/tmp/deploy/images/sama5d2-xplained-sd/at91-sama5d2_xplained.dtb \
    -f ${OE_BUILD_DIR}/tmp/deploy/images/sama5d2-xplained-sd/zImage
