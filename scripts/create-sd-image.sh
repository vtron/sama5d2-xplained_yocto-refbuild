#!/bin/sh

set -e

SCRIPT_DIR=`dirname $0`

DEVICE_TYPE=sama5d2-xplained
ROOTFS=
CONFIG_FS=
OUTPUT_FILE=/opt/${DEVICE_TYPE}-image/${DEVICE_TYPE}-sdimage.img
#OUTPUT_FILE=
TMP_DIR=/opt/${DEVICE_TYPE}-image/build-${DEVICE_TYPE}-sdimage
# add files to a dos boot partition if listed
VFAT_PARTITION_FILES=
CREATE_VFAT_PARTITION=0

decho() {
    echo "    `date +"%Y/%m/%d %H:%M:%S"` $*"
}

print_usage() {
    echo "Usage: $0 options"
    echo "  -r <${DEVICE_TYPE}-rootfs.tar.gz>  : The ${DEVICE_TYPE}'s root filesystem archive - eg /tmp/build-${DEVICE_TYPE}"
    echo "  -c <config-partition-fs.tar.gz     : [optional] The config partition(2) file archive to be installed"
    echo "  -f <src:dest-name>                 : File to add to the leading VFAT partition. Can add multiple times."
    echo "  -o <output-file.img>               : The output filename : default /opt/${DEVICE_TYPE}-image/${DEVICE_TYPE}-sdimage.img"
    echo "  -t <tmp_dir>                       : The temporary dir to perform operations : default: /opt/${DEVICE_TYPE}-image/build-${DEVICE_TYPE}-sdimage"
}

while getopts "r:c:f:o:t:?" opt; do
    case $opt in
        "r")
            ROOTFS=$OPTARG
            ;;
        "c")
            CONFIG_FS=$OPTARG
            ;;
        "f")
            CREATE_VFAT_PARTITION=1
            VFAT_PARTITION_FILES="${VFAT_PARTITION_FILES}${OPTARG} "
            ;;
        "o")
            OUTPUT_FILE=$OPTARG
            ;;
        "t")
            TMP_DIR=$OPTARG
            ;;
        "*")
            print_usage
            exit 1
            ;;
    esac
done

error_usage_exit() {
    echo "ERROR: $*"
    echo ""
    print_usage
    exit 1
}

[ -z "$ROOTFS" ] && error_usage_exit "no rootfs provided"
[ -z "$OUTPUT_FILE" ] && error_usage_exit "no output file provided"

IAM=`whoami`
[ $IAM != "root" ] && echo "Must be run as root to use os rootfs" && exit 1
    
if [ $CREATE_VFAT_PARTITION -eq 1 ] && [ -n "$VFAT_PARTITION_FILES" ]; then
    for i in $VFAT_PARTITION_FILES ; do
        SRC=$(echo -n $i | cut -d ':' -f 1)
        ( [ -z "$SRC" ] || [ ! -f "$SRC" ] ) && error_usage_exit "Failed to find boot partition file: $SRC"
    done
fi

    
mkdir -p $TMP_DIR
mkdir -p `dirname $OUTPUT_FILE`

# Partition Size (Meg). All other sizes generated from this.
#PARTITION_SIZE_MB=910
PARTITION_SIZE_MB=512
PARTITION_VFAT_SIZE_MB=256
PARTITION_VFAT_OFFS_MB=1
PARTITION1_OFFS_MB=280

# partitioning done in 'sectors' which are 512 bytes
let "PARTITION_VFAT_SIZE = $PARTITION_VFAT_SIZE_MB * 1024 * 1024"
let "PARTITION_VFAT_OFFS_SECTORS = ($PARTITION_VFAT_OFFS_MB * 1024 * 1024) / 512"
let "PARTITION_VFAT_SIZE_SECTORS = $PARTITION_VFAT_SIZE / 512"

let "PARTITION_SIZE = $PARTITION_SIZE_MB * 1024 * 1024"
let "PARTITION_SIZE_SECTORS = $PARTITION_SIZE / 512"
let "IMAGE_SIZE_MB = ($PARTITION_SIZE_MB * 2)  + 50"
[ $CREATE_VFAT_PARTITION -eq 1 ] && let "IMAGE_SIZE_MB = ($PARTITION_SIZE_MB * 2) + $PARTITION1_OFFS_MB + 50"

let "PARTITION2_OFFS_MB = $PARTITION1_OFFS_MB + $PARTITION_SIZE_MB"
let "PARTITION1_OFFS_SECTORS = ($PARTITION1_OFFS_MB * 1024 * 1024) / 512"
let "PARTITION2_OFFS_SECTORS = $PARTITION1_OFFS_SECTORS + $PARTITION_SIZE_SECTORS"

if [ $CREATE_VFAT_PARTITION -eq 1 ]; then
    decho "  boot(VFAT) partition size:    ${PARTITION_VFAT_SIZE_MB}MB, $PARTITION_VFAT_SIZE_SECTORS sectors"
fi

decho "  partition size:    ${PARTITION_SIZE_MB}MB, ${PARTITION_SIZE} bytes, $PARTITION_SIZE_SECTORS sectors"
decho "  image size:        ${IMAGE_SIZE_MB}MB"
decho "  partition offsets: p1:${PARTITION1_OFFS_MB}MB ($PARTITION1_OFFS_SECTORS sectors)"
decho "                     p2:${PARTITION2_OFFS_MB}MB ($PARTITION2_OFFS_SECTORS sectors)"

# Create empty image file
decho "$OUTPUT_FILE : Creating"
dd if=/dev/zero of=$OUTPUT_FILE bs=1M count=$IMAGE_SIZE_MB 2>/dev/null


# Partition file
if [ $CREATE_VFAT_PARTITION -eq 0 ]; then

    decho "$OUTPUT_FILE : Partitioning"
    sfdisk $OUTPUT_FILE > $TMP_DIR/sfdisk.log 2>&1 << EOFLINUX
# partition table of /dev/sdx
unit: sectors

/dev/sdx1 : start= $PARTITION1_OFFS_SECTORS, size= $PARTITION_SIZE_SECTORS, Id=83
/dev/sdx2 : start= $PARTITION2_OFFS_SECTORS, size= $PARTITION_SIZE_SECTORS, Id=83
/dev/sdx3 : start=        0, size=        0, Id= 0
/dev/sdx4 : start=        0, size=        0, Id= 0
EOFLINUX

else

    decho "$OUTPUT_FILE : Partitioning with VFAT partition"
    sfdisk $OUTPUT_FILE > $TMP_DIR/sfdisk.log 2>&1 << EOFVFAT
# partition table of /dev/sdx
unit: sectors

/dev/sdx1 : start= $PARTITION_VFAT_OFFS_SECTORS, size= $PARTITION_VFAT_SIZE_SECTORS, Id=0b, bootable
/dev/sdx2 : start= $PARTITION1_OFFS_SECTORS, size= $PARTITION_SIZE_SECTORS, Id=83
/dev/sdx3 : start= $PARTITION2_OFFS_SECTORS, size= $PARTITION_SIZE_SECTORS, Id=83
/dev/sdx4 : start=        0, size=        0, Id= 0
EOFVFAT

fi

# Create partitions & format
echo -n > $TMP_DIR/mkfs.log
if [ $CREATE_VFAT_PARTITION -eq 1 ]; then
    decho "Partition-0 (VFAT) : Creating"
    dd if=/dev/zero of=$TMP_DIR/sdimage.img0 bs=1M count=$PARTITION_VFAT_SIZE_MB 2>/dev/null
    mkfs.vfat -F 32 -n "boot" $TMP_DIR/sdimage.img0 >> $TMP_DIR/mkfs.log 2>&1
fi

decho "Partition-1 : Creating"
dd if=/dev/zero of=$TMP_DIR/sdimage.img1 bs=1M count=$PARTITION_SIZE_MB 2>/dev/null
mkfs.ext4 -F -L "rootfs" $TMP_DIR/sdimage.img1 >> $TMP_DIR/mkfs.log 2>&1

decho "Partition-2 : Creating"
dd if=/dev/zero of=$TMP_DIR/sdimage.img2 bs=1M count=$PARTITION_SIZE_MB 2>/dev/null
mkfs.ext4 -F -L "configfs" $TMP_DIR/sdimage.img2 >> $TMP_DIR/mkfs.log 2>&1

# Mount as loopback & extract rootfs
mkdir -p $TMP_DIR/mnt

# vfat boot partition
PART0_USAGE=
if [ $CREATE_VFAT_PARTITION -eq 1 ]; then
    decho "Partition-0 (VFAT) : Installing filesystem"
    mount -o loop $TMP_DIR/sdimage.img0 $TMP_DIR/mnt/
    for i in $VFAT_PARTITION_FILES ; do
        SRC=$(echo -n $i | cut -d ':' -f 1)
        DEST=$(echo -n $i | cut -s -d ':' -f 2)
        [ -z "$DEST" ] && DEST=$(basename $SRC)
        cp -v $SRC $TMP_DIR/mnt/$DEST
    done
    PART0_USAGE=$(df -h $TMP_DIR/mnt/)
    umount $TMP_DIR/mnt/
fi

# root partition
decho "Partition-1 : Installing filesystem-1 : $ROOTFS"
mount -o loop $TMP_DIR/sdimage.img1 $TMP_DIR/mnt/
tar xzf $ROOTFS -C $TMP_DIR/mnt/
rm -rf $TMP_DIR/mnt/lost+found
PART1_USAGE=$(df -h $TMP_DIR/mnt/)
umount $TMP_DIR/mnt/

# config partition
decho "Partition-2 : Installing filesystem-2 : $CONFIG_FS"
mount -o loop $TMP_DIR/sdimage.img2 $TMP_DIR/mnt/
[ -n "$CONFIG_FS" ] && tar xzf $CONFIG_FS -C $TMP_DIR/mnt/
rm -rf $TMP_DIR/mnt/lost+found
PART2_USAGE=$(df -h $TMP_DIR/mnt/)
umount $TMP_DIR/mnt/

# Copy the partitions into the sd image at the correct locations (matches the sfdisk offsets above)
if [ $CREATE_VFAT_PARTITION -eq 1 ]; then
    decho "$OUTPUT_FILE : Installing Partition-0 (VFAT)"
    dd if=$TMP_DIR/sdimage.img0 of=$OUTPUT_FILE bs=1M seek=$PARTITION_VFAT_OFFS_MB conv=notrunc 2>/dev/null
fi

decho "$OUTPUT_FILE : Installing Partition-1"
dd if=$TMP_DIR/sdimage.img1 of=$OUTPUT_FILE bs=1M seek=$PARTITION1_OFFS_MB conv=notrunc 2>/dev/null

decho "$OUTPUT_FILE : Installing Partition-2"
dd if=$TMP_DIR/sdimage.img2 of=$OUTPUT_FILE bs=1M seek=$PARTITION2_OFFS_MB conv=notrunc 2>/dev/null

rm -f $TMP_DIR/sdimage.img0 $TMP_DIR/sdimage.img1 $TMP_DIR/sdimage.img2
rmdir $TMP_DIR/mnt

sync

[ $CREATE_VFAT_PARTITION -eq 1 ] && decho "Partition 0 Usage: $PART0_USAGE (VFAT)"
decho "Partition 1 Usage: $PART1_USAGE"
decho "Partition 2 Usage: $PART2_USAGE"

decho "Complete : $OUTPUT_FILE"
