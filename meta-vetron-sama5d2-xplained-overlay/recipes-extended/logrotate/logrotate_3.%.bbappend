FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " file://cron.logrotate "

do_install_append() {
    mkdir -p ${D}${sysconfdir}/cron.daily
    mkdir -p ${D}${sysconfdir}/cron.hourly
    install -m 0755 ${WORKDIR}/cron.logrotate ${D}${sysconfdir}/cron.daily/logrotate
    install -m 0755 ${WORKDIR}/cron.logrotate ${D}${sysconfdir}/cron.hourly/logrotate
}

