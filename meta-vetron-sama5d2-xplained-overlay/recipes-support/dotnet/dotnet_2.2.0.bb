SUMMARY = "Dotnet Core for Linux"
HOMEPAGE = "https://github.com/dotnet"
SECTION = "libs"

DEPENDS = "libunwind zlib openssl icu curl"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

SRC_URI = "https://dotnetcli.blob.core.windows.net/dotnet/Runtime/release/2.2/dotnet-runtime-latest-linux-arm.tar.gz"
SRC_URI[md5sum] = "8701d5c0aaad2b22b0100410184d1285"
# SRC_URI[sha512sum] = "BA2DA77A6DD186FC26E61706813A38D4854D8DC703066949E2D2130613DEDD9F07CAE76128EA331608ACA0D500CDA2D59A41F1C2B44956494E5AC4C3D6C4FA03"

