
PR = "r0.1"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " file://50_ntpconf "

do_install_append() {
    install -d ${D}${sysconfdir}/default/volatiles
    install -m 0755    ${WORKDIR}/50_ntpconf       ${D}${sysconfdir}/default/volatiles
    rm -f ${D}/${sysconfdir}/ntp.conf
}

FILES_${PN} += "${sysconfdir}/default/volatiles/50_ntpconf"

