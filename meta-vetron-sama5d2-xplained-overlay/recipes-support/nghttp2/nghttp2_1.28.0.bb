SUMMARY = "HTTP/2 C Library and tools"
HOMEPAGE = "https://nghttp2.org/"
SECTION = "libs"

DEPENDS = "pkgconfig zlib openssl boost "

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

SRC_URI = "https://github.com/nghttp2/nghttp2/releases/download/v${PV}/nghttp2-${PV}.tar.bz2"
SRC_URI[md5sum] = "ec61b4c9244d81b4bb703e75c13af073"
SRC_URI[sha256sum] = "8b7ea0acb6719129798c892c05fd0eb26b02dc5d48e713c5d9496f797652a17f"

inherit autotools pythonnative python-dir

EXTRA_OECONF = " --enable-lib-only=yes "

do_install_append() {
    rmdir --ignore-fail-on-non-empty ${D}${bindir}
}

