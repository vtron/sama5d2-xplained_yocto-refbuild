IMAGE_FEATURES += "ssh-server-dropbear package-management"

IMAGE_INSTALL = "\
    kernel-image \
    kernel-devicetree \
    kernel-modules \
    packagegroup-core-boot \
    packagegroup-base-wifi \
    packagegroup-base-bluetooth \
    packagegroup-base-usbgadget \
    lrzsz \
    setserial \
    \
    nbench-byte \
    lmbench \
    \
    alsa-utils \
    i2c-tools \
    devmem2 \
    dosfstools \
    libdrm-tests \
    mtd-utils \
    mtd-utils-ubifs \
    dtc \
    dtc-misc \
    iproute2 \
    iptables \
    bridge-utils \
    can-utils \
    evtest \
    gdbserver \
    usbutils \
    u-boot-fw-utils \
    wget \
    \
    libunwind \
    icu \
    \
    \
    ${CORE_IMAGE_EXTRA_INSTALL} \
    "

#    linux-firmware \
#

inherit core-image
