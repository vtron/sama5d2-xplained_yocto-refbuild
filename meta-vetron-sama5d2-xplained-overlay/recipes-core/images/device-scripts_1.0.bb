SUMMARY = "Display device patches and scripts"
DESCRIPTION = "Display device patches and scripts"
SECTION = "base"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=751419260aa954499f7abaabaa882bbe"
PR = "r1"

INHIBIT_DEFAULT_DEPS = "1"

SRC_URI = " \
    file://COPYING \
    file://device_early_boot \
    file://device-hostname \
    file://iptables-persistent \
    file://rules.v4 \
    file://rules.v6 \
    file://udhcpc_ntp.update.script \
    file://udhcpc_env.dump.script \
    "

S = "${WORKDIR}"

KERNEL_VERSION = ""

inherit update-alternatives
DEPENDS_append = " update-rc.d-native"
DEPENDS_append = " ${@bb.utils.contains('DISTRO_FEATURES','systemd','systemd-systemctl-native','',d)}"
# Add dependency to any conf/scripts we modify/overwrite
DEPENDS += " initscripts init-ifupdown "
#DEPENDS += " iproute2 ntp busybox-syslog "
DEPENDS += " iproute2 ntp "

RDEPENDS_${PN} += " initscripts init-ifupdown "
RDEPENDS_${PN} += " iproute2 ntp busybox-syslog "
#RDEPENDS_${PN} += " iproute2 ntp "


do_install () {
#
# Create directories and install device independent scripts
#
	install -d ${D}${sysconfdir}/init.d
	install -d ${D}${sysconfdir}/rcS.d
	install -d ${D}${sysconfdir}/rc0.d
	install -d ${D}${sysconfdir}/rc1.d
	install -d ${D}${sysconfdir}/rc2.d
	install -d ${D}${sysconfdir}/rc3.d
	install -d ${D}${sysconfdir}/rc4.d
	install -d ${D}${sysconfdir}/rc5.d
	install -d ${D}${sysconfdir}/rc6.d
	install -d ${D}${sysconfdir}/default
	install -d ${D}${sysconfdir}/iptables
	install -d ${D}${sysconfdir}/udhcpc.d
	install -d ${D}${sysconfdir}/device-early-boot.d
	install -d ${D}/usr/local/bin

	install -m 0755    ${WORKDIR}/iptables-persistent	${D}${sysconfdir}/init.d
	install -m 0755    ${WORKDIR}/device_early_boot	${D}${sysconfdir}/init.d

	install -m 0644    ${WORKDIR}/rules.v4		${D}${sysconfdir}/iptables
	install -m 0644    ${WORKDIR}/rules.v6		${D}${sysconfdir}/iptables
	install -m 0644    ${WORKDIR}/device-hostname		${D}${sysconfdir}

	install -m 0755 ${WORKDIR}/udhcpc_env.dump.script ${D}${sysconfdir}/udhcpc.d/55envdump
	install -m 0755 ${WORKDIR}/udhcpc_ntp.update.script ${D}${sysconfdir}/udhcpc.d/60ntpupdate

# Create runlevel links
#
	update-rc.d -r ${D} iptables-persistent start 37 2 3 4 5 .
	update-rc.d -r ${D} device_early_boot start 37 S .
}

pkg_postinst_${PN} () {
    # clean-up
    rm -rf "$D"/usr/share/sounds
}

FILES_${PN} += "${prefix}/local/bin"

