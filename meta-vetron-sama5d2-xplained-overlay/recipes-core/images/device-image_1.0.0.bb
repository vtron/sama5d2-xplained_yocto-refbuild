DESCRIPTION = "An image for network and communication."
LICENSE = "MIT"
PR = "r1"

require device-image.inc

IMAGE_FEATURES += " ssh-server-dropbear"

IMAGE_INSTALL += "\
    packagegroup-base-usbhost \
    busybox-syslog \
    logrotate \
    cronie \
    cpio \
    gzip \
    bzip2 \
    zip unzip \
    p7zip \
    openssl \
    wget \
    curl \
    jq \
    rsync \
    openssh-sftp-server \
    dropbear \
    e2fsprogs dosfstools \
    ethtool \
    ntp ntpdate \
    ldd \
    strace \
    tcpdump \
    file \
    avahi-utils avahi-autoipd avahi-daemon \
    nginx \
    device-scripts \
    nghttp2 \
    "

# Dev Libs
IMAGE_INSTALL += "\
    boost \
    lmdb \
    sqlite3 \
    "
